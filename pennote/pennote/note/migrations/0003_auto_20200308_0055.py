# Generated by Django 3.0.3 on 2020-03-08 00:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('note', '0002_auto_20200306_0508'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='note',
            name='editors',
        ),
        migrations.RemoveField(
            model_name='note',
            name='readers',
        ),
    ]
