from django.db import models

import uuid

from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User

from pennote.checklist.models import ChecklistItem


class Tag(models.Model):
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid.uuid4, editable=False)
    tag = models.TextField('name', null=False)
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT, related_name='tags')

    class Meta:
        unique_together = [['tag', 'owner']]


class Note(models.Model):    
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid.uuid4, editable=False)
    title = models.TextField('title', null=False)
    content = models.TextField('content', null=False)
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT, related_name='notes')
    # editors = models.ManyToManyField(User, related_name='note_editors')
    # readers = models.ManyToManyField(User, related_name='note_readers')
    checklist = models.ForeignKey(ChecklistItem, null=True, on_delete=models.PROTECT, related_name='notes')
    tags = models.ManyToManyField(Tag, related_name='notes')

