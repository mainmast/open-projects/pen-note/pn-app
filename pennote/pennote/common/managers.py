from django.db import models


class OwnerManager(models.Manager):
    def users_objects(self, user):
        return super().get_queryset().filter(owner=user)


class EditorReaderManager(models.Manager):
    def users_objects(self, user):
        return super().get_queryset().filter(
            models.Q(owner=user) | 
            models.Q(editors__in=[user]) |
            models.Q(readers__in=[user])
        )


class ProjectEditorReaderManager(models.Manager):
    def users_objects(self, user):
        return super().get_queryset().filter(
            models.Q(owner=user) | 
            models.Q(editors__in=[user]) |
            models.Q(readers__in=[user]) | 
            models.Q(project__editors__in=[user]) |
            models.Q(project__readers__in=[user])
        )
