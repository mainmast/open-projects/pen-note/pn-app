from rest_framework import permissions

class IsSecuredAPIUser(permissions.BasePermission):
    """
    Global permission check to see if the authenticated user is using a secured JWT
    """

    def has_permission(self, request, view):
        secured_jwt = request.get_cookie('SECURED_JWT_SESSION')
        print(secured_jwt)

        return True
