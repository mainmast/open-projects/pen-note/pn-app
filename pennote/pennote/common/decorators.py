from rest_framework.exceptions import PermissionDenied


def is_request_owner(func):

    def wrapper(*args, **kwargs):
        view = args[0]
        request = args[1]

        if view.get_object().owner != request.user:
            raise PermissionDenied("Nope... Not yours!")

        return func(*args, **kwargs)
    
    return wrapper
        