from rest_framework import serializers

class OwnerSerialiser(serializers.ModelSerializer):

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        
        if request and hasattr(request, "user"):
            user = request.user

        validated_data['owner'] = user

        return super().create(validated_data)