from django.db import models

import uuid

from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User

from pennote.common.managers import OwnerManager
from pennote.project.models import Project


class Checklist(models.Model):    
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField('name', null=False)
    description = models.TextField('description', null=False)
    image = models.TextField('image', null=True)
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT, related_name='checklists')
    # editors = models.ManyToManyField(User, related_name='checklist_editors')
    # readers = models.ManyToManyField(User, related_name='checklist_readers')
    project = models.ForeignKey(Project, on_delete=models.PROTECT)

    objects = OwnerManager()


class ChecklistItem(models.Model):    
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField('name', null=False)
    description = models.TextField('description', null=False)
    done = models.BooleanField('done', default=False)
    checklist = models.ForeignKey(Checklist, null=False, on_delete=models.PROTECT, related_name='checklist_items')    
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT, related_name='checklist_items')

    objects = OwnerManager()