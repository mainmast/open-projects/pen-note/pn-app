from django_filters import FilterSet
from django_filters.rest_framework import DjangoFilterBackend

from django.utils.decorators import decorator_from_middleware

from rest_framework import filters
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from ..models import Checklist, ChecklistItem
from .serialisers import ChecklistItemCreateListSerializer, ChecklistSerializer, ChecklistItemResourceSerializer

from ...common.decorators import is_request_owner


class ChecklistFilter(FilterSet):
    class Meta:
        model = Checklist
        fields = ['project', 'project__name']


class ChecklistListCreate(ListAPIView, CreateAPIView):
    """
    get:
    Lists all checklists available for a given user.

    post:
    Creates a new checklist for the authenticated user.

    """
    queryset = Checklist.objects.all()
    serializer_class = ChecklistSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    filter_class = ChecklistFilter
    permission_classes = [IsAuthenticated,]

    def list(self, request, *args, **kwargs):
        self.queryset = Checklist.objects.users_objects(request.user)
        return super().list(request, *args, **kwargs)
    
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class ChecklistItemListCreate(ListAPIView, CreateAPIView):
    """
    get:
    Lists all checklist items available for a given user.

    post:
    Creates a new checklist item for the authenticated user.

    """
    queryset = Checklist.objects.all()
    serializer_class = ChecklistItemCreateListSerializer
    permission_classes = [IsAuthenticated,]

    def list(self, request, *args, **kwargs):
        checklist = self.get_object()

        self.queryset = ChecklistItem.objects.users_objects(request.user).filter(
            checklist=checklist
        )
        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):        
        return super().create(request, *args, **kwargs)


class ChecklistItemResource(RetrieveUpdateDestroyAPIView):
    """
    get:
    Gets the specified checklist item resource.

    patch:
    Updates the specified checklist item resource.

    put:
    Updates the specified checklist item resource.

    delete:
    Deletes the specified checklist item resource.
    """
    queryset = ChecklistItem.objects.all()
    serializer_class = ChecklistItemResourceSerializer
    permission_classes = [IsAuthenticated,]

    @is_request_owner
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(self, request, *args, **kwargs)
    

    @is_request_owner
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)