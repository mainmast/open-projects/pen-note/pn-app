from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from ...common.serialisers import OwnerSerialiser

from ..models import Checklist
from ..models import ChecklistItem


class ChecklistSerializer(OwnerSerialiser):

    class Meta:
        model = Checklist
        fields = ['uuid', 'name', 'description', 'image', 'owner', 'project']
        read_only_fields = ['uuid', 'owner']


class ChecklistItemCreateListSerializer(OwnerSerialiser):
    def validate(self, attrs):
        # We need to set the project to the PK in the url if we have one.
        request = self.context.get("request")
        checklist = request.parser_context.get('kwargs').get('pk')

        if checklist and not attrs.get('checklist'):
            checklist_obj = Checklist.objects.users_objects.filter(pk=checklist).first()
            attrs['checklist'] = checklist_obj
        
        elif checklist and attrs.get('checklist') and checklist != str(attrs.get('checklist').uuid):
            raise ValidationError(detail="Endpoint checklist and item body checklist differ")
        
        return super().validate(attrs)

    def create(self, validated_data):
        return super().create(validated_data)

    class Meta:
        model = ChecklistItem
        fields = ['uuid', 'name', 'description', 'done', 'owner', 'checklist']
        read_only_fields = ['uuid', 'owner']


class ChecklistItemResourceSerializer(OwnerSerialiser):
    class Meta:
        model = ChecklistItem
        fields = ['uuid', 'name', 'description', 'done', 'owner', 'checklist']
        read_only_fields = ['uuid', 'owner', 'checklist']