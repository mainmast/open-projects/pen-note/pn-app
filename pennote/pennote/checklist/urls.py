from django.conf.urls import url

from .api import views


urlpatterns = [
    url(
        regex=r'^$',
        view=views.ChecklistListCreate.as_view(),
        name='checklists'
    ),

    url(
        regex=r'^(?P<pk>[a-z0-9\-]+)/items/$',
        view=views.ChecklistItemListCreate.as_view(),
        name='checklist-items'
    ),
    
    url(
        regex=r'^items/(?P<pk>[a-z0-9\-]+)/$',
        view=views.ChecklistItemResource.as_view(),
        name='checklist-item-resource'
    ),
]
