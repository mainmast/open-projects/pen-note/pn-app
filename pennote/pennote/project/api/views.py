from django.utils.decorators import decorator_from_middleware

from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated

from ..models import Project
from .serialisers import ProjectSerializer


class ProjectListCreate(ListAPIView, CreateAPIView):
    """
    get:
    Lists all projects available for a given user.

    post:
    Creates a new project for the authenticated user.

    """
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [IsAuthenticated,]

    def list(self, request, *args, **kwargs):
        self.queryset = Project.objects.users_objects(request.user)
        return super().list(request, *args, **kwargs)
    
    def create(self, request, *args, **kwargs):
        

        return super().create(request, *args, **kwargs)
