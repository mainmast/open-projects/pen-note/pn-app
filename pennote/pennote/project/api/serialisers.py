from pennote.common.serialisers import OwnerSerialiser
from ..models import Project


class ProjectSerializer(OwnerSerialiser):

    
    class Meta:
        model = Project
        fields = ['uuid', 'name', 'description', 'image', 'owner',]
        read_only_fields = ['uuid', 'owner',]
