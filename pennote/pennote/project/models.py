from django.db import models

import uuid

from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User

from pennote.common.managers import OwnerManager


class Project(models.Model):    
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField('name', null=False)
    description = models.TextField('description', null=False)
    image = models.TextField('image', null=True)
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT)
    # editors = models.ManyToManyField(User, blank=True, related_name='project_editors')
    # readers = models.ManyToManyField(User, blank=True, related_name='project_readers')

    objects = OwnerManager()
