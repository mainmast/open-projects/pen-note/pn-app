"""pennote URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import uuid

from django.contrib import admin
from django.urls import path
from django.conf.urls import include


from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from pennote.project.api.views import ProjectListCreate
from pennote.checklist.api.views import ChecklistListCreate, ChecklistItemListCreate


class SecureTokenView(TokenObtainPairView):

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        response.set_cookie(key='SECURED_JWT_SESSION', value=uuid.uuid4().hex, httponly=True, domain="localhost")
        return response

    
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', SecureTokenView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/projects/', ProjectListCreate.as_view(), name='projects'),

    path('api/checklists/', include('pennote.checklist.urls'), name='checklists'),
    
]