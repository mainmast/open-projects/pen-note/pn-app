from django.db import models

import uuid

from django.utils import timezone
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User


class Organisation(models.Model):    
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField('name', null=False)
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT, related_name='organisations')
